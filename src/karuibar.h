/* karuibar - a lightweight status bar for X11
 *
 * Copyright (C) 2015 Tinu Weber <takeya@bluewin.ch>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _KARUIBAR_H
#define _KARUIBAR_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/* macros */
#define BUFSIZE 512
#define LENGTH(A) (sizeof(A)/sizeof(A[0]))
#define MAX(A, B) ((A) > (B) ? (A) : (B))
#define MIN(A, B) ((A) < (B) ? (A) : (B))

/* logging */
#define DEBUG(...) print(stdout, LOG_DEBUG, __VA_ARGS__)
#define VERBOSE(...) print(stdout, LOG_VERBOSE, __VA_ARGS__)
#define NOTE(...) print(stdout, LOG_NORMAL, __VA_ARGS__)
#define WARN(...) print(stderr, LOG_WARN, __VA_ARGS__)
#define ERROR(...) print(stderr, LOG_ERROR, __VA_ARGS__)

/* log verbosity levels */
enum log_level { LOG_FATAL, LOG_ERROR, LOG_WARN, LOG_NORMAL, LOG_VERBOSE,
                 LOG_DEBUG };

/* module structure */
struct module {
	/* callback functions */
	int (*init)(struct module *);
	int (*poll)(struct module *);
	int (*react)(struct module *);
	void (*term)(struct module *);

	/* look */
	uint32_t fg, bg;

	/* module data (name, icons, file descriptor, custom data) */
	char const *basename;
	char name[BUFSIZE];
	struct icon **icons;
	size_t nicon;
	int fd;
	int hide_on_error;
	void *data;

	/* shared library handler */
	void *so_handler;

	/* display buffer */
	char buf[BUFSIZE];
};

/* Append formatted string to given buffer. */
void buf_append(char const *format, ...) __attribute__((format(printf, 1, 2)));

/* Empty given buffer. */
void buf_clear(void);

/* Get a colour representation for a certain value. */
uint32_t colour(int unsigned val) __attribute__((warn_unused_result));

/* Print formatted message to given file descriptor. */
void print(FILE *out, enum log_level, char const *format, ...)
     __attribute__((format(printf, 3, 4)));

/* Register an icon, get back icon ID. */
int register_icon(int long unsigned const *bitfield)
    __attribute__((warn_unused_result));

/* Memory allocation that guarantees to return a valid pointer (it crashes
 * internally with an error message according to passed context otherwise).
 * sfree(...) is just there so you might get around including stdlib ;-)
 */
void *scalloc(size_t nmemb, size_t size, char const *context)
      __attribute__((warn_unused_result));
void *smalloc(size_t size, char const *context)
      __attribute__((warn_unused_result));
void *srealloc(void *ptr, size_t size, char const *context)
      __attribute__((warn_unused_result));
void sfree(void *);

/* Get values from X resources (for string, assume buffer length >= BUFSIZE). */
int xresources_boolean(char const *name, int *ret, int def);
int xresources_colour(char const *name, uint32_t *ret, uint32_t def);
int xresources_integer(char const *name, int *ret, int def);
int xresources_string(char const *name, char *ret, char const *def);

/* Register as master module. */
int register_master(void);

/* Set bar visibility (only if master module). */
int set_visible(int visible);

/* karuibar configuration */
struct {
	struct {
		char font[BUFSIZE];
		uint32_t foreground, background;
		uint32_t emphasised, faded, error, warning, separator;
		int unsigned wpad, wsep;
		bool bottom;
		int unsigned interval;
		int unsigned xgap; /* FIXME either dimensions, or remove entirely*/
		size_t npaths;
		char **paths;
	} xresources;
	struct {
		char const *HOME;
		char HOSTNAME[BUFSIZE];
	} env;
	int unsigned w, h;
} karuibar;

#endif /* _KARUIBAR_H */
