/* karuibar - a lightweight status bar for X11
 *
 * Copyright (C) 2015 Tinu Weber <takeya@bluewin.ch>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#define _POSIX_SOURCE    /* for strtok */
#define _XOPEN_SOURCE 500 /* for unistd.h */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <locale.h>
#include <sys/time.h>
#include <dlfcn.h>
#include <X11/Xlib.h>
#include <X11/Xresource.h>
#include "karuibar.h"
#ifdef XINERAMA
# include <X11/extensions/Xinerama.h>
#endif

/* macros */
#define APPNAME "karuibar"
#define FATAL(...) print(stderr, LOG_FATAL, __VA_ARGS__)
#define DIE(R) exit(R)
#define MODULE_PATH "share/"APPNAME"/modules"

/* structures */
struct {
	int unsigned ascent, descent, height;
	XFontStruct *xfontstruct;
	XFontSet xfontset;
} font;

struct group {
	struct module **modules;
	size_t nmod;
	Pixmap pm;
	int unsigned w;
};

struct icon {
	int unsigned w, h;
	Pixmap pm;
	GC gc;
};

struct monitor {
	int x, y;
	int unsigned w, h;
	Window barwin;
	Pixmap barpm;
};

struct {
	int unsigned x; /* X position (pixel) */
	int unsigned c; /* character position */
	uint32_t fg, bg;
} pen;

/* functions */
static void attach(void ***, size_t *, void *, size_t, char const *);
static void attach_group(struct group *);
static void attach_icon(struct icon *);
static void attach_module(struct group *, struct module *);
static void attach_monitor(struct monitor *);
static void block(struct timeval);
static void detach(void ***, size_t *, void *, size_t, char const *);
static void detach_group(struct group *);
static void detach_module(struct group *, struct module *);
static void detach_monitor(struct monitor *);
static void draw_icon(struct group *, struct icon *);
static void draw_padding(struct group *);
static void draw_separator(struct group *);
static void draw_string(struct group *, char const *);
static void escape(struct group *, char const *);
static void events(fd_set *);
static void *find_shared(char const *name);
static int init(void) __attribute__((warn_unused_result));
static void init_env(void);
static int init_font(char const *) __attribute__((warn_unused_result));
static struct group *init_group(char const *)
                     __attribute__((warn_unused_result));
static struct icon *init_icon(int long unsigned const *)
                    __attribute__((warn_unused_result));
static struct module *init_module(char const *)
                      __attribute__((warn_unused_result));
static void init_modules(void);
static struct monitor *init_monitor(int, int, int unsigned, int unsigned)
                       __attribute__((warn_unused_result));
static void init_sighandler(void);
static void init_xresources(void);
static void *load_shared(char const *path);
static void *load_shared_function(void *so_handler, char const *name);
static void poll(void);
static void redraw(void);
static void redraw_barwin(struct monitor *);
static void redraw_group(struct group *);
static void run(void);
static void scan_monitors(void);
#ifdef XINERAMA
static void scan_monitors_xinerama(void);
#endif
static int set_log_level(int, char **);
static void sighandler(int);
static char *strdupf(char const *format, ...)
            __attribute__((format(printf, 1, 2)));
static ssize_t strpos(char const *, char);
static char const *strsig(int) __attribute__((warn_unused_result));
static int unsigned strwidth(char const *, size_t)
                    __attribute__((warn_unused_result));
static void stumble(struct module *, char const *);
static void term(void);
static void term_group(struct group *);
static void term_icon(struct icon *);
static void term_module(struct module *);
static void term_monitor(struct monitor *);
static int vstrlenf(char const *format, va_list ap);
static void xresources_key(char *buf, char const *name, size_t buflen, bool host);
static void xevent(void);

/* variables */
static Display *dpy;
static int screen;
static Window root;
static int depth;
static GC gc;
static Visual *visual;
static int running, restarting;
static struct monitor **monitors;
static struct group **groups, *selgrp;
static struct module *selmod, *master;
static size_t nmon, ngrp;
static XrmDatabase xdb;
static Colormap cm;
static int xfd;
static enum log_level log_level;
static int bottom, visible;

/* implementation */
static void
attach(void ***l, size_t *lsize, void *e, size_t esize, char const *context)
{
	*l = srealloc(*l, ++(*lsize)*esize, context);
	*(*l+*lsize-1) = e;
}

static void
attach_group(struct group *grp)
{
	attach((void ***) &groups, &ngrp, grp, sizeof(grp), "group list");
}

static void
attach_icon(struct icon *icon)
{
	attach((void ***) &selmod->icons, &selmod->nicon, icon, sizeof(icon),
	        "icon list");
}

static void
attach_module(struct group *grp, struct module *mod)
{
	attach((void ***) &grp->modules, &grp->nmod, mod, sizeof(mod),
	       "module list");
}

static void
attach_monitor(struct monitor *mon)
{
	attach((void ***) &monitors, &nmon, mon, sizeof(mon), "monitor list");
}

static void
block(struct timeval tick)
{
	int unsigned g, m;
	int s;
	fd_set fds;

	do {
		FD_ZERO(&fds);
		FD_SET(xfd, &fds);
		for (g = 0; g < ngrp; ++g) {
			selgrp = groups[g];
			for (m = 0; m < selgrp->nmod; ++m) {
				if (selgrp->modules[m]->fd < 0)
					continue;
				FD_SET(selgrp->modules[m]->fd, &fds);
			}
			selgrp = NULL;
		}
		s = select(FD_SETSIZE, &fds, NULL, NULL, &tick);
		if (s <= 0) {
			if (running && s < 0)
				NOTE("select() interrupted");
			break;
		}
		events(&fds);
		redraw();
	} while (tick.tv_sec > 0 || tick.tv_usec > 0);
}

void
buf_append(char const *format, ...)
{
	va_list args;
	size_t len = strlen(selmod->buf);

	va_start(args, format);
	(void) vsnprintf(selmod->buf+len, BUFSIZE-len, format, args);
	va_end(args);
}

inline void
buf_clear(void)
{
	selmod->buf[0] = '\0';
}

uint32_t
colour(int unsigned val)
{
	int ival;

	val = MIN(100, val);
	ival = (int) val - 100;
	return (uint32_t) (((int)((1.0-val*val*val/1000000.0)*255)<<16)+
	                   ((int)((1.0-ival*ival*ival*(-1)/1000000.0)*255)<<8)+
	                   51);
}

static void
detach(void ***l, size_t *lsize, void *e, size_t esize, char const *context)
{
	int unsigned i;

	for (i = 0; i < *lsize && *(*l+i) != e; ++i);
	for (; i < *lsize-1; ++i)
		*(*l+i) = *(*l+i+1);
	*l = srealloc(*l, --(*lsize)*esize, context);
}

static void
detach_group(struct group *grp)
{
	detach((void ***) &groups, &ngrp, grp, sizeof(grp), "group list");
}

static void
detach_module(struct group *grp, struct module *mod)
{
	detach((void ***) &grp->modules, &grp->nmod, mod, sizeof(mod),
	       "module list");
}

static void
detach_monitor(struct monitor *mon)
{
	detach((void ***) &monitors, &nmon, mon, sizeof(mon), "monitor list");
}

static void
draw_icon(struct group *grp, struct icon *icon)
{
	(void) XSetForeground(dpy, gc, pen.fg);
	(void) XSetBackground(dpy, gc, pen.bg);
	(void) XCopyPlane(dpy, icon->pm, grp->pm, gc, 0, 0, icon->w, icon->h,
	                  (int) pen.x, MAX(0, (int) (karuibar.h - icon->h - 1)),
	                  1);
	pen.x += icon->w;
}

static void
draw_padding(struct group *grp)
{
	if (karuibar.xresources.wpad == 0)
		return;
	(void) XSetForeground(dpy, gc, pen.bg);
	(void) XFillRectangle(dpy, grp->pm, gc, (int) pen.x, 0,
	                      karuibar.xresources.wpad, karuibar.h);
	pen.x += karuibar.xresources.wpad;
}

static void
draw_separator(struct group *grp)
{
	(void) XSetForeground(dpy, gc, karuibar.xresources.separator);
	(void) XFillRectangle(dpy, grp->pm, gc, (int) pen.x, 0,
	                      karuibar.xresources.wsep, karuibar.h);
	pen.x += karuibar.xresources.wsep;
}

static void
draw_string(struct group *grp, char const *str)
{
	char const *s = str+pen.c;
	int unsigned nc, sw, sh = font.height+2, y = font.ascent+1;
	size_t slen = strlen(s);

	/* determine number of characters and width */
	for (nc = 0; nc < slen && s[nc] != 0x1B; ++nc);
	sw = strwidth(s, nc);

	/* clear background */
	XSetForeground(dpy, gc, pen.bg);
	XFillRectangle(dpy, grp->pm, gc, (int) pen.x, 0, sw, sh);

	/* draw string */
	XSetForeground(dpy, gc, pen.fg);
	if (font.xfontset != NULL)
		Xutf8DrawString(dpy, grp->pm, font.xfontset, gc,
		                (int) pen.x, (int) y, s, (int) nc);
	else
		XDrawString(dpy, grp->pm, gc, (int) pen.x, (int) y,
		            s, (int) nc);

	/* update pen */
	pen.c += nc;
	pen.x += sw;

	/* check for escape sequences */
	if (str[pen.c] == 0x1B)
		escape(grp, str);

	/* check for additional text */
	if (pen.c < strlen(str))
		draw_string(grp, str);
}

static void
escape(struct group *grp, char const *str)
{
	int unsigned i, nc = 0;
	char const *op = str+pen.c+1;
	uint32_t tmp;

	switch (*op) {
	case 'B': /* Background colour */
		if (sscanf(op+1, "{%06x}%n", &pen.bg, (int *) &nc) < 1)
			WARN("invalid background escape sequence");
		break;
	case 'e': /* Error */
		pen.fg = karuibar.xresources.error;
		break;
	case 'F': /* Foreground colour */
		if (sscanf(op+1, "{%06x}%n", &pen.fg, (int *) &nc) < 1)
			WARN("invalid foreground escape sequence");
		break;
	case 'f': /* Faded */
		pen.fg = karuibar.xresources.faded;
		break;
	case 'I': /* Icon */
		if (sscanf(op+1, "{%u}%n", &i, (int *) &nc) < 1)
			WARN("invalid icon escape sequence");
		else if (i >= selmod->nicon)
			WARN("out-of-bounds icon index (>=%zu)", selmod->nicon);
		else
			draw_icon(grp, selmod->icons[i]);
		break;
	case 'm': /* eMphasised */
		pen.fg = karuibar.xresources.emphasised;
		break;
	case 'n': /* Negate (inverse) colour */
		tmp = pen.fg;
		pen.fg = pen.bg;
		pen.bg = tmp;
		break;
	case 'p': /* Padding */
		draw_padding(grp);
		break;
	case 'r': /* Reset colours */
		if (selmod != NULL) {
			pen.fg = selmod->fg;
			pen.bg = selmod->bg;
		} else {
			pen.fg = karuibar.xresources.foreground;
			pen.bg = karuibar.xresources.background;
		}
		break;
	case 's': /* Separator */
		draw_separator(grp);
		break;
	case 'w':
		pen.fg = karuibar.xresources.warning;
		break;
	default:
		WARN("invalid escape sequence: [\\033%c...]", *op);
	}
	pen.c += nc+2;
}

static void
events(fd_set *fds)
{
	int unsigned g, m;

	/* X event */
	if (FD_ISSET(xfd, fds))
		xevent();

	/* module event */
	for (g = 0; g < ngrp; ++g) {
		selgrp = groups[g];
		for (m = 0; m < selgrp->nmod; ++m) {
			selmod = selgrp->modules[m];
			if (selmod->fd < 0 || !FD_ISSET(selmod->fd, fds))
				continue;
			if (selmod->react == NULL) {
				ERROR("no event handler specified");
				stumble(selmod, "react_error");
			}
			if (selmod->react(selmod) < 0) {
				ERROR("failed to react");
				stumble(selmod, "react_error");
			}
			selmod = NULL;
		}
		selgrp = NULL;
	}
}

static bool
file_exists(char const *path)
{
	FILE *f;

	if (path == NULL)
		return false;
	f = fopen(path, "r");
	if (f == NULL)
		return false;
	fclose(f);
	return true;
}

static void *
find_shared(char const *name)
{
	int unsigned i;
	char sopath[BUFSIZE];
	void *so_handler;

	for (i = 0; i < karuibar.xresources.npaths; ++i) {
		snprintf(sopath, BUFSIZE, "%s/%s.so",
		         karuibar.xresources.paths[i], name);
		VERBOSE("checking for shared object at %s", sopath);
		if (!file_exists(sopath))
			continue;
		so_handler = load_shared(sopath);
		if (so_handler != NULL)
			return so_handler;
	}

	return NULL;
}

static int
init(void)
{
	XSetWindowAttributes wa;
	XGCValues xgcv;

	/* signals */
	init_sighandler();

	/* locale, environment */
	if (!setlocale(LC_ALL, "") || !XSupportsLocale()) {
		FATAL("could not set locale");
		return -1;
	}
	init_env();

	/* X display */
	dpy = XOpenDisplay(NULL);
	if (dpy == NULL) {
		FATAL("could not open X");
		return -1;
	}

	/* X properties */
	xfd = ConnectionNumber(dpy);
	screen = DefaultScreen(dpy);
	depth = DefaultDepth(dpy, screen);
	visual = DefaultVisual(dpy, screen);
	cm = DefaultColormap(dpy, screen);

	/* X resources */
	init_xresources();

	/* root window */
	root = RootWindow(dpy, screen);
	xgcv.graphics_exposures = False;
	gc = XCreateGC(dpy, root, GCGraphicsExposures, &xgcv);
	wa.event_mask = StructureNotifyMask;
	XChangeWindowAttributes(dpy, root, CWEventMask, &wa);

	/* fonts */
	if (init_font(karuibar.xresources.font) < 0) {
		FATAL("could not initialise fonts");
		return -1;
	}
	karuibar.h = font.height+2;

	/* monitors */
	nmon = 0;
	scan_monitors();

	/* modules */
	ngrp = 0;
	init_modules();

	return 0;
}

static void
init_env(void)
{
	karuibar.env.HOME = getenv("HOME");
	gethostname(karuibar.env.HOSTNAME, BUFSIZE);
}

static int
init_font(char const *fontstr)
{
	int n;
	XFontStruct **xfonts;
	char **xfontnames;
	char *def, **missing=NULL;

	font.xfontset = XCreateFontSet(dpy, fontstr, &missing, &n, &def);
	if (missing != NULL) {
		while (n--)
			VERBOSE("missing fontset: %s", missing[n]);
		XFreeStringList(missing);
	}

	/* if fontset loads successfully, get info; otherwise dummy font */
	if (font.xfontset != NULL) {
		font.ascent = font.descent = 0;
		n = XFontsOfFontSet(font.xfontset, &xfonts, &xfontnames);
		while (n--) {
			font.ascent = MAX(font.ascent,
			                  (int unsigned) xfonts[n]->ascent);
			font.descent = MAX(font.descent,
			                   (int unsigned) xfonts[n]->descent);
		}
	} else {
		WARN("fallback to XLoadQueryFont");
		font.xfontstruct = XLoadQueryFont(dpy, fontstr);
		if (font.xfontstruct == NULL) {
			/* all hope is gone */
			FATAL("cannot load font '%s'", fontstr);
			return -1;
		}
		font.ascent = (int unsigned) font.xfontstruct->ascent;
		font.descent = (int unsigned) font.xfontstruct->descent;
	}
	font.height = font.ascent + font.descent;
	return 0;
}

static struct group *
init_group(char const *xrstr)
{
	char *saveptr, grpstr[BUFSIZE];
	char const *modtok, *del = ", ";

	selgrp = smalloc(sizeof(struct group), "group");
	selgrp->modules = NULL;
	selgrp->nmod = 0;
	selgrp->pm = XCreatePixmap(dpy, root, karuibar.w, karuibar.h,
	                           (int unsigned) depth);

	strcpy(grpstr, xrstr);
	modtok = strtok_r(grpstr, del, &saveptr);
	while (modtok != NULL) {
		selmod = init_module(modtok);
		if (selmod == NULL)
			WARN("could not initialise module '%s'", modtok);
		else
			attach_module(selgrp, selmod);
		selmod = NULL;
		modtok = strtok_r(NULL, del, &saveptr);
	}
	if (selgrp->nmod == 0) {
		WARN("skipping empty module group");
		term_group(selgrp);
		selgrp = NULL;
	}
	return selgrp;
}

static struct icon *
init_icon(int long unsigned const *bitfield)
{
	int unsigned x, y;
	struct icon *icon;
	XGCValues xgcv;

	icon = smalloc(sizeof(struct icon), "icon");
	icon->w = (int unsigned) bitfield[0];
	icon->h = (int unsigned) bitfield[1];
	icon->pm = XCreatePixmap(dpy, root, icon->w, icon->h, 1);

	xgcv.graphics_exposures = False;
	icon->gc = XCreateGC(dpy, icon->pm, GCGraphicsExposures, &xgcv);

	XSetForeground(dpy, icon->gc, 0);
	XFillRectangle(dpy, icon->pm, icon->gc, 0, 0, icon->w, icon->h);
	XSetForeground(dpy, icon->gc, 1);
	for (y = 0; y < icon->h; ++y)
		for (x = 0; x < icon->w; ++x)
			if ((bitfield[y+2]>>(icon->w-x-1))&1)
				XDrawPoint(dpy, icon->pm, icon->gc,
				           (int) x, (int) y);
	return icon;
}

static struct module *
init_module(char const *name)
{
	char basename[BUFSIZE], initname[BUFSIZE];
	ssize_t psep;

	psep = strpos(name, '_');
	if (psep < 0) {
		strcpy(basename, name);
	} else {
		strncpy(basename, name, MIN((size_t) psep, BUFSIZE));
		basename[psep] = '\0';
	}
	basename[BUFSIZE - 1] = '\0';
	snprintf(initname, BUFSIZE, "%s_init", basename);
	initname[BUFSIZE - 1] = '\0';

	/* create module */
	selmod = smalloc(sizeof(struct module), "module");
	strcpy(selmod->name, name);
	selmod->poll = NULL;
	selmod->react = NULL;
	selmod->term = NULL;
	selmod->buf[0] = '\0';
	selmod->nicon = 0;
	selmod->fd = -1;
	selmod->icons = NULL;
	(void) xresources_colour("foreground", &selmod->fg, karuibar.xresources.foreground);
	(void) xresources_colour("background", &selmod->bg, karuibar.xresources.background);
	(void) xresources_boolean("hide_on_error", &selmod->hide_on_error, false);

	/* load module */
	selmod->so_handler = find_shared(basename);
	if (selmod->so_handler == NULL) {
		ERROR("failed to load shared object");
		stumble(selmod, "so_error");
		return selmod;
	}

	*(void **) &selmod->init = load_shared_function(selmod->so_handler,
	                                                initname);
	/* load module's init function */
	if (selmod->init == NULL) {
		ERROR("failed to load init function from shared object");
		stumble(selmod, "so_fnc_error");
		return selmod;
	}

	/* initialise module */
	if (selmod->init(selmod) < 0) {
		ERROR("failed to initialise");
		stumble(selmod, "init_error");
	}

	return selmod;
}

static void
init_modules(void)
{
	char xrstr[BUFSIZE], *saveptr;
	char const *grptok, *del = ";";

	xresources_string("modules", xrstr, "");

	grptok = strtok_r(xrstr, del, &saveptr);
	while (grptok != NULL) {
		selgrp = init_group(grptok);
		if (selgrp == NULL)
			WARN("could not initialise module group '%s'", grptok);
		else
			attach_group(selgrp);
		selgrp = NULL;
		grptok = strtok_r(NULL, del, &saveptr);
	}
	if (ngrp == 0)
		WARN("running "APPNAME" without module groups!");
}

static struct monitor *
init_monitor(int x, int y, int unsigned w, int unsigned h)
{
	XSetWindowAttributes wa;
	size_t s = sizeof(struct monitor);
	struct monitor *mon = smalloc(s, "monitor");
	int yoff;

	VERBOSE("detected monitor at %ux%u%+d%+d", w, h, x, y);

	mon->x = x;
	mon->y = y;
	mon->w = w;
	mon->h = h;
	yoff = bottom ? mon->y + (int) mon->h - (int) karuibar.h : 0;

	/* pixmap, to reduce flickering */
	mon->barpm = XCreatePixmap(dpy, root, mon->w - karuibar.xresources.xgap,
	                           karuibar.h, (int unsigned) depth);

	/* bar window */
	wa.override_redirect = True;
	wa.event_mask = ExposureMask;
	mon->barwin = XCreateWindow(dpy, root,
	                            mon->x + (int) karuibar.xresources.xgap,
	                            mon->y + yoff,
	                            mon->w - karuibar.xresources.xgap,
	                            font.height + 2,
	                            0, /* border */
	                            depth, CopyFromParent, visual,
	                            CWOverrideRedirect | CWEventMask, &wa);
	if (visible)
		XMapWindow(dpy, mon->barwin);
	return mon;
}

static void
init_paths(void)
{
	char xrstr[BUFSIZE], ***paths = &karuibar.xresources.paths;
	char *pathtok;

	karuibar.xresources.npaths = 0;
	xresources_string("paths", xrstr, "");
	pathtok = strtok(xrstr, ", ");
	while (pathtok != NULL) {
		++karuibar.xresources.npaths;
		*paths = srealloc(*paths,
		                  karuibar.xresources.npaths*sizeof(char *),
		                  "module paths");
		(*paths)[karuibar.xresources.npaths - 1] = pathtok;
		pathtok = strtok(NULL, ", ");
	}
}

static void
init_sighandler(void)
{
	int unsigned i;
	int sigs[] = { SIGINT, SIGTERM, SIGUSR1, SIGUSR2 };

	for (i = 0; i < LENGTH(sigs); ++i)
		if (signal(sigs[i], sighandler) == SIG_ERR)
			WARN("could not set up signal handler for %s (%d): %s",
			     strsig(sigs[i]), sigs[i], strerror(errno));
}

static void
init_xresources(void)
{
	char *xrm;
	char pathstr[BUFSIZE];

	char ***paths = &karuibar.xresources.paths;
	size_t *npaths = &karuibar.xresources.npaths;

	/* get X resources manager values */
	xrm = XResourceManagerString(dpy);
	if (xrm == NULL) {
		WARN("could not get X resources manager string");
		return;
	}

	XrmInitialize();
	xdb = XrmGetStringDatabase(xrm);

	/* user configuration */
	(void) xresources_colour("foreground", &karuibar.xresources.foreground, 0xAAAAAA);
	(void) xresources_colour("background", &karuibar.xresources.background, 0x222222);
	(void) xresources_colour("faded",      &karuibar.xresources.faded,      0x555555);
	(void) xresources_colour("emphasised", &karuibar.xresources.emphasised, 0xEEEEEE);
	(void) xresources_colour("warning",    &karuibar.xresources.warning,    0xFFFF00);
	(void) xresources_colour("error",      &karuibar.xresources.error,      0xFF0000);
	(void) xresources_colour("separator",  &karuibar.xresources.separator,  0x555555);
	(void) xresources_integer("interval", (int *) &karuibar.xresources.interval, 2);
	(void) xresources_integer("wpad", (int *) &karuibar.xresources.wpad, 8);
	(void) xresources_integer("wsep", (int *) &karuibar.xresources.wsep, 1);
	(void) xresources_boolean("bottom", &bottom, 0);
	(void) xresources_integer("xgap", (int *) &karuibar.xresources.xgap, 0);
	(void) xresources_string("font", karuibar.xresources.font,
	     "-misc-fixed-medium-r-semicondensed--13-120-75-75-c-60-iso8859-1");

	/* user configuration (module paths) */
	karuibar.xresources.npaths = 0;
	(void) xresources_string("path", pathstr, "");
	init_paths();
	*npaths += 3;
	*paths = srealloc(*paths, *npaths*sizeof(char *), "module path list");
	(*paths)[*npaths - 3] = strdupf("%s/.local/"MODULE_PATH,
	                                karuibar.env.HOME);
	(*paths)[*npaths - 2] = strdupf("/usr/local/"MODULE_PATH);
	(*paths)[*npaths - 1] = strdupf("/usr/"MODULE_PATH);
}

static void *
load_shared(char const *path)
{
	void *so_handler;
	char *dl_error;

	so_handler = dlopen(path, RTLD_NOW);
	dl_error = dlerror();
	if (dl_error != NULL) {
		WARN("%s", dl_error);
		return NULL;
	}
	return so_handler;
}

static void *
load_shared_function(void *so_handler, char const *name)
{
	void *fnptr;
	char *dl_error;

	fnptr = dlsym(so_handler, name);
	dl_error = dlerror();
	if (dl_error != NULL) {
		WARN("%s", dl_error);
		return NULL;
	}
	return fnptr;

}

static void
poll(void)
{
	int unsigned g, m;

	for (g = 0; g < ngrp; ++g) {
		selgrp = groups[g];
		for (m = 0; m < selgrp->nmod; ++m) {
			selmod = selgrp->modules[m];
			if (selmod->poll == NULL)
				continue;
			if (selmod->poll(selmod) < 0) {
				ERROR("failed to poll");
				stumble(selmod, "poll_error");
			}
		}
	}
	selmod = NULL;
}

void
print(FILE *f, enum log_level level, char const *format, ...)
{
	va_list args;
	time_t rawtime;
	struct tm *date;
	char const *col;
	char timebuf[BUFSIZE];

	/* ignore messages with higher verbosity level */
	if (level > log_level)
		return;

	/* timestamp */
	rawtime = time(NULL);
	date = localtime(&rawtime);
	(void) strftime(timebuf, BUFSIZE, "%Y-%m-%dT%H:%M:%S", date);
	(void) fprintf(f, APPNAME" %s ", timebuf);

	/* module name */
	if (selmod != NULL)
		(void) fprintf(f, "[%s] ", selmod->name);

	/* log level */
	switch (level) {
	case LOG_FATAL: col="\033[31mFATAL\033[0m "; break;
	case LOG_ERROR: col="\033[31mERROR\033[0m "; break;
	case LOG_WARN : col="\033[33mWARN\033[0m " ; break;
	case LOG_DEBUG: col="\033[34mDEBUG\033[0m "; break;
	default: col="";
	}
	(void) fprintf(f, "%s", col);

	/* message */
	va_start(args, format);
	(void) vfprintf(f, format, args);
	va_end(args);

	(void) fprintf(f, "\n");
	(void) fflush(f);
}

static void
redraw(void)
{
	int unsigned i;

	for (i = 0; i < ngrp; ++i)
		redraw_group(groups[i]);
	for (i = 0; i < nmon; ++i)
		redraw_barwin(monitors[i]);
	XFlush(dpy);
}

static void
redraw_barwin(struct monitor *mon)
{
	int unsigned i, w, h, x, occ;
	int gap;

	w = mon->w - karuibar.xresources.xgap;
	h = karuibar.h;

	/* clear bar */
	XSetForeground(dpy, gc, karuibar.xresources.background);
	XFillRectangle(dpy, mon->barpm, gc, 0, 0, w, h);

	/* calculate occupied/free space */
	for (i = 0, gap = (int) w; i < ngrp; ++i)
		gap -= (int) groups[i]->w;
	gap = MAX(gap, 0);

	/* copy module group pixmap(s) */
	if (ngrp == 1)
		XCopyArea(dpy, groups[0]->pm, mon->barpm, gc, 0, 0,
		          groups[0]->w, h, gap, 0);
	else if (ngrp > 1)
		for (i = 0, occ = 0; i < ngrp; ++i) {
			x = i * (int unsigned) gap / ((int unsigned) ngrp - 1)
			    + occ;
			XCopyArea(dpy, groups[i]->pm, mon->barpm, gc, 0, 0,
			          groups[i]->w, h, (int) x, 0);
			occ += groups[i]->w;
		}

	/* copy to window */
	XCopyArea(dpy, mon->barpm, mon->barwin, gc, 0, 0, w, h, 0, 0);
}

static void
redraw_group(struct group *grp)
{
	int unsigned i;
	int first = 1;

	/* clear pixmap */
	XSetForeground(dpy, gc, karuibar.xresources.background);
	XFillRectangle(dpy, grp->pm, gc, 0, 0, karuibar.w, karuibar.h);

	pen.x = 0;
	selgrp = grp;
	for (i = 0; i < grp->nmod; ++i) {
		/* skip empty buffers */
		if (grp->modules[i]->buf[0] == '\0')
			continue;

		/* separator */
		if (!first && grp->modules[i-1]->bg == grp->modules[i]->bg) {
			pen.fg = karuibar.xresources.faded;
			draw_separator(grp);
		}
		pen.fg = grp->modules[i]->fg;
		pen.bg = grp->modules[i]->bg;
		pen.c = 0;

		/* module buffer */
		selmod = grp->modules[i];
		draw_padding(grp);
		draw_string(grp, selmod->buf);
		draw_padding(grp);
		selmod = NULL;

		first = 0;
	}
	grp->w = pen.x;
	selgrp = NULL;
}

int
register_icon(int long unsigned const *bitfield)
{
	attach_icon(init_icon(bitfield));
	return (int) selmod->nicon-1;
}

int
register_master(void)
{
	if (master != NULL) {
		WARN("attempt to register as master (conflict with %s)",
		     master->name);
		return -1;
	}
	master = selmod;
	return 0;
}

static void
run(void)
{
	struct timeval tick = {
		.tv_sec = karuibar.xresources.interval,
		.tv_usec = 0,
	};

	while (running) {
		redraw();
		poll();
		block(tick);
	}
}

void *
scalloc(size_t nmemb, size_t size, char const *context)
{
	void *m = calloc(nmemb, size);

	if (m == NULL) {
		FATAL("could not allocate %zu*%zu bytes for %s",
		      nmemb, size, context);
		DIE(EXIT_FAILURE);
	}
	return m;
}

static void
scan_monitors(void)
{
	struct monitor *mon;

	/* clear monitor list */
	while (nmon > 0) {
		mon = monitors[0];
		detach_monitor(mon);
		term_monitor(mon);
	}

#ifdef XINERAMA
	if (XineramaIsActive(dpy)) {
		scan_monitors_xinerama();
		return;
	}
#endif

	mon = init_monitor(0, 0, (int unsigned) DisplayWidth(dpy, screen),
	                         (int unsigned) DisplayHeight(dpy, screen));
	karuibar.w = mon->w;
	attach_monitor(mon);
}

#ifdef XINERAMA
static void
scan_monitors_xinerama(void)
{
	int dup;
	int unsigned raw_n, n, raw_i, i;
	struct monitor *mon;
	XineramaScreenInfo *raw_info, *info;

	/* get screen information */
	raw_info = XineramaQueryScreens(dpy, (int *) &raw_n);

	/* de-duplicate screen information */
	info = scalloc(raw_n, sizeof(XineramaScreenInfo),
	               "Xinerama screen info");
	n = 0;
	for (raw_i = 0; raw_i < raw_n; ++raw_i) {
		dup = 0;
		for (i = 0; i < n && !dup; ++i) {
			dup = raw_info[raw_i].x_org == info[i].x_org &&
			      raw_info[raw_i].y_org == info[i].y_org &&
			      raw_info[raw_i].width == info[i].width &&
			      raw_info[raw_i].height == info[i].height;
		}
		if (dup)
			continue;
		info[n].x_org = raw_info[raw_i].x_org;
		info[n].y_org = raw_info[raw_i].y_org;
		info[n].width = raw_info[raw_i].width;
		info[n].height = raw_info[raw_i].height;
		++n;
	}
	XFree(raw_info);

	/* create one monitor for each screen */
	karuibar.w = 0;
	for (i = 0; i < n; ++i) {
		mon = init_monitor(info[i].x_org, info[i].y_org,
		                   (int unsigned) info[i].width,
		                   (int unsigned) info[i].height);
		attach_monitor(mon);
		karuibar.w = MAX(karuibar.w, mon->w);
	}
	XFree(info);

	XSync(dpy, screen);
}
#endif /* XINERAMA */

static int
set_log_level(int argc, char **argv)
{
	log_level = LOG_NORMAL;
	if (argc == 1)
		return 0;
	if (strcmp(argv[1], "-v") == 0)
		log_level = LOG_VERBOSE;
	else if (strcmp(argv[1], "-d") == 0)
		log_level = LOG_DEBUG;
	else if (strcmp(argv[1], "-q") == 0)
		log_level = LOG_FATAL;
	if (argc != 2 || log_level == LOG_NORMAL) {
		(void) printf("Usage: "APPNAME" [-v|-d|-q]\n");
		return -1;
	}
	return 0;
}

int
set_visible(int v)
{
	int unsigned i;

	if (selmod != NULL && selmod != master) {
		WARN("attempt from non-master module to set visibility");
		return -1;
	}
	visible = v;
	if (visible)
		for (i = 0; i < nmon; ++i)
			XMapWindow(dpy, monitors[i]->barwin);
	else
		for (i = 0; i < nmon; ++i)
			XUnmapWindow(dpy, monitors[i]->barwin);
	return 0;
}

inline void
sfree(void *ptr)
{
	free(ptr);
}

static void
sighandler(int sig)
{
	struct module *m = selmod;

	selmod = NULL;
	NOTE("received %s (%d)", strsig(sig), sig);
	switch (sig) {
	case SIGUSR2:
		set_visible(!visible);
		init_sighandler();
		break;
	case SIGUSR1:
		restarting = 1;
		__attribute__((fallthrough));
	default:
		running = 0;
	}
	selmod = m;
}

void *
smalloc(size_t size, char const *context)
{
	void *m = malloc(size);

	if (m == NULL) {
		FATAL("could not allocate %zu bytes for %s", size, context);
		DIE(EXIT_FAILURE);
	}
	return m;
}

void *
srealloc(void *ptr, size_t size, char const *context)
{
	void *m;

	if (size == 0) {
		free(ptr);
		return NULL;
	}
	m = realloc(ptr, size);
	if (m == NULL) {
		FATAL("could not reallocate %zu bytes for %s", size, context);
		DIE(EXIT_FAILURE);
	}
	return m;
}

static char *
strdupf(char const *format, ...)
{
	int len;
	size_t slen;
	char *dst;
	va_list ap;

	va_start(ap, format);
	len = vstrlenf(format, ap);
	va_end(ap);

	if (len < 0)
		return NULL;
	slen = (size_t) len + 1;

	dst = smalloc(slen, "formatted string duplication");
	va_start(ap, format);
	len = vsnprintf(dst, slen, format, ap);
	va_end(ap);

	if (len < 0) {
		sfree(dst);
		return NULL;
	}

	return dst;
}

static ssize_t
strpos(char const *haystack, char needle)
{
	ssize_t i, len = (ssize_t) strlen(haystack);

	for (i = 0; i < len; ++i)
		if (haystack[i] == needle)
			return i;
	return -1;
}

static char const *
strsig(int sig)
{
	switch (sig) {
	case SIGTERM: return "SIGTERM";
	case SIGINT : return "SIGINT";
	case SIGUSR1: return "SIGUSR1";
	case SIGUSR2: return "SIGUSR2";
	default: return "unknown";
	}
}

static int unsigned
strwidth(char const *str, size_t len)
{
	if (font.xfontset != NULL)
		return (int unsigned) Xutf8TextEscapement(font.xfontset, str,
		                                          (int) len);
	else
		return (int unsigned) XTextWidth(font.xfontstruct, str,
		                                 (int) len);
}

static void
stumble(struct module *mod, char const *msg)
{
	if (selmod->term)
		selmod->term(selmod);
	selmod->init = NULL;
	selmod->poll = NULL;
	selmod->react = NULL;
	selmod->term = NULL;
	selmod->fd = -1;
	selmod->fg = karuibar.xresources.error;
	selmod->bg = karuibar.xresources.background;
	buf_clear();
	if (!selmod->hide_on_error)
		buf_append("%s:%s", mod->name, msg);
}

static void
term(void)
{
	struct monitor *mon;

	/* modules */
	while (ngrp > 0) {
		selgrp = groups[0];
		detach_group(selgrp);
		term_group(selgrp);
	}

	/* font */
	if (font.xfontset != NULL)
		XFreeFontSet(dpy, font.xfontset);
	else
		XFreeFont(dpy, font.xfontstruct);

	/* monitors */
	while (nmon > 0) {
		mon = monitors[0];
		detach_monitor(mon);
		term_monitor(mon);
	}

	/* X resources, X display */
	XrmDestroyDatabase(xdb);
	XFreeGC(dpy, gc);
	XCloseDisplay(dpy);
}

static void
term_group(struct group *grp)
{
	while (grp->nmod > 0) {
		selmod = grp->modules[0];
		detach_module(selgrp, selmod);
		term_module(selmod);
		selmod = NULL;
	}
	XFreePixmap(dpy, grp->pm);
	free(grp);
}

static void
term_icon(struct icon *icon)
{
	XFreeGC(dpy, icon->gc);
	XFreePixmap(dpy, icon->pm);
	free(icon);
}

static void
term_module(struct module *mod)
{
	int unsigned i;

	selmod = mod;
	if (mod->term != NULL)
		mod->term(mod);
	for (i = 0; i < mod->nicon; ++i)
		term_icon(mod->icons[i]);
	if (mod->so_handler != NULL)
		dlclose(mod->so_handler);
	free(mod->icons);
	free(mod);
	selmod = NULL;
}

static void
term_monitor(struct monitor *mon)
{
	XUnmapWindow(dpy, mon->barwin);
	XDestroyWindow(dpy, mon->barwin);
	free(mon);
}

static int
vstrlenf(char const *format, va_list ap)
{
	int len;
	char *buf = NULL;

	len = vsnprintf(buf, 0, format, ap);
	return len;
}

static void
xevent(void)
{
	XEvent xev;

	if (!XCheckMaskEvent(dpy, ExposureMask|StructureNotifyMask, &xev)) {
		WARN("unmasked X event");
		return;
	}
	switch (xev.type) {
	case Expose:
		redraw();
		break;
	case ConfigureNotify:
		if (xev.xconfigure.window == root) {
			VERBOSE("root window properties have changed");
			scan_monitors();
		}
		break;
	default:
		WARN("masked but unhandled X event: %d", xev.type);
	}
}

int
xresources_boolean(char const *name, int *ret, int def)
{
	char str[BUFSIZE];

	if (xresources_string(name, str, "") < 0) {
		*ret = def;
		return -1;
	}
	*ret = strcasecmp(str, "true") == 0 || strcasecmp(str, "1") == 0;
	return 0;
}

int
xresources_colour(char const *name, uint32_t *ret, uint32_t def)
{
	XColor xcolour;
	char str[BUFSIZE];

	if (xresources_string(name, str, "") < 0) {
		*ret = def;
		return -1;
	}
	if (XAllocNamedColor(dpy, cm, str, &xcolour, &xcolour) == 0) {
		WARN("resource '%s' carries invalid colour value", name);
		return -1;
	}
	*ret = (uint32_t) xcolour.pixel;
	return 0;
}

int
xresources_integer(char const *name, int *ret, int def)
{
	char str[BUFSIZE];

	if (xresources_string(name, str, "") < 0) {
		*ret = def;
		return -1;
	}
	*ret = (int) strtol(str, NULL, 0);
	return 0;
}

static void
xresources_key(char *buf, char const *name, size_t buflen, bool host)
{
	(void) snprintf(buf, buflen, "%s%s%s.%s%s%s", APPNAME,
	                host ? "_" : "", host ? karuibar.env.HOSTNAME : "",
	                selmod != NULL && selmod->name != NULL ? selmod->name : "",
	                selmod != NULL && selmod->name != NULL ? "." : "", name);
}

int
xresources_string(char const *name, char *ret, char const *def)
{
	char rname[BUFSIZE] = { 0 };
	char *type = NULL;
	XrmValue val = { 0, NULL };

	/* host-specific */
	xresources_key(rname, name, BUFSIZE, true);
	if (XrmGetResource(xdb, rname, "*", &type, &val))
		goto xresources_string_success;

	/* generic */
	xresources_key(rname, name, BUFSIZE, false);
	if (XrmGetResource(xdb, rname, "*", &type, &val))
		goto xresources_string_success;

	/* default */
	strncpy(ret, def, BUFSIZE);
	return -1;

 xresources_string_success:
	strncpy(ret, val.addr, BUFSIZE);
	return 0;
}

int
main(int argc, char **argv)
{
	running = 1;
	restarting = 0;
	visible = 1;

	if (set_log_level(argc, argv) < 0)
		DIE(EXIT_FAILURE);

	if (init() < 0) {
		FATAL("initialisation failed");
		DIE(EXIT_FAILURE);
	}
	run();
	term();

	if (restarting) {
		NOTE("restarting "APPNAME);
		execvp(APPNAME, argv);
	}
	return EXIT_SUCCESS;
}
