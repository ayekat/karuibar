karuibar
========

karuibar is a lightweight, hackable, modular status bar for displaying various
information about the system at the screen's top or bottom edge.

![screenshot](scrot/groups3.png)

See [karuibar-modules](https://gitlab.com/ayekat/karuibar-modules) for how to
write your own modules, and a collection of already existing modules.


dependencies
------------

* Xlib
* Xinerama *(optional)*


build
-----

	make

In case you haven't got Xinerama:

	make noxinerama


install
-------

	make install

will install karuibar into `/usr/local`.

Pass the `BINDIR` environment variable to change the binary install location (by
default `/usr/local/bin`), and `HEADERDIR` to change the header install location
(by default `/usr/local/include/karuibar`)

As a shorthand option, passing only `INSTALLDIR` has the following effect:

* `BINDIR` is set to `INSTALLDIR/bin`
* `HEADERDIR` is set to `INSTALLDIR/include/karuibar`


uninstall
---------

In a general case, a simple `make uninstall` suffices. You may need to adapt
`INSTALLDIR`/`BINDIR`/`HEADERDIR` as seen in the [*install*](#install) section.


usage
-----

	karuibar [-v|-d|-q]

`-v` will enable verbose output, and `-d` even more verbose, debug output.
Conversely, `-q` will omit any output other than fatal errors.


configuration
-------------

karuibar's configuration takes place in the [X
resources](http://en.wikipedia.org/wiki/X_resources) file (usually
`$HOME/.Xresources`:

X resource            | Description
:---------------------|:--------------------------------------------------------
`karuibar.modules`    | List of modules that are initialised and shown in the status bar. See the [#modules](#modules) section for more information about how to handle modules, and the syntax syntax of this list.
`karuibar.font`       | Font that is used in the status bar; takes an X11 font name (although apparently it is also possible to [specify it differently](http://blog.yjl.im/2014/11/karuibar-lightweight-status-bar.html)) (default: `-misc-fixed-medium-r-semicondensed--13-120-75-75-c-60-iso8859-1`).
`karuibar.foreground` | Generic foreground colour (default: `#AAAAAA`).
`karuibar.background` | Generic background colour (default: `#222222`).
`karuibar.emphasised` | Foreground colour for emphasised text (default: `#EEEEEE`).
`karuibar.faded`      | Foreground colour for faded out text (default: `#555555`).
`karuibar.error`      | Foreground colour for error messages (default: `#FF0000`).
`karuibar.separator`  | Colour for module separator (default: `#555555`).
`karuibar.wpad`       | Padding width in pixels at each module's edge (default: 8).
`karuibar.wsep`       | Separator width in pixels (default: 1). Note that the separator is printed *between* modules, but not before the first or after the last module.
`karuibar.bottom`     | Status bar is shown at the screen's bottom if set to `true`, otherwise at the screen's top (default: false).
`karuibar.xgap`       | gap in pixels between the status bar window and the screen's left edge (default: 0). **NOTE:** this setting will be removed in the future, it is currently just a hack'ish workaround.
`karuibar.interval`   | interval in seconds between two poll requests (default: 2).
`karuibar.paths`      | Comma separated list of paths in which karuibar will search for modules *in addition* to the default ones (`$HOME/.local/share/karuibar/modules`, `/usr/local/share/karuibar/modules` and `/usr/share/karuibar/modules`)

Once modified, you need to update the X resource database as follows:

	xrdb ~/.Xresources


### modules

Each module gathers and displays information about different things. Depending
on your preferences and needs, you may enable or disable them, and arrange them
however you like.

Essentially, karuibar is just a blank status bar that provides a C header that
allows people to write their modules to display desired things.

There is a repository of officially supported and maintained modules at
[karuibar-modules](https://gitlab.com/ayekat/karuibar-modules), so even if you
don't know programming, it's very likely that the module you search is already
there.

There are three things that need to be done in order to get a module showing
things:

1. compile and install the corresponding module
2. add an entry to the `karuibar.modules` list in the X resources, and reload
   the X resources file
3. (re)launch karuibar

#### arrangement

The `modules` list is basically just a list of module names. It may look like
this:

	karuibar.modules : memory, cpu, alsa, time

By default, they will all get stacked up the the right, like this:

![groups1](scrot/groups1.png)

In case you also want to show something on the left side (e.g. the cpu module),
it may be achieved by separating the modules by a semicolon (instead of a
comma):

	karuibar.modules: cpu; memory, alsa, time

The semicolon acts like a padded out area in the status bar, and it will look
like this:

![groups2](scrot/groups2.png)

You can define an arbitary number of module groups, and they will get evenly
distributed on the bar; let's add the `battery` module in the middle:

	karuibar.modules: cpu; battery; memory, alsa, time

![groups3](scrot/groups3.png)

#### module configuration

Modules may be configured through additional X resource names. See their
specific readmes, if existing. Alternatively, if passing the verbose flag `-v`
to karuibar, each unset X resource is printed to stdout, e.g.:

	karuibar [2014-10-09,23:16:03] [time] karuibar.time.foreground: <default>

#### profiles

In case you want to use a module multiple times, but let each instance behave
differently, you can assign profile names to a module instance, and configure
each profile separately.

Profiles are set by simply appending an underscore `_` and the profile name to
the module name.

To have a more practical example, the `time` module takes a configuration option
`format` for the [date format](http://linux.die.net/man/1/date). Let's assume
you want to display two time modules, one displaying the date and one displaying
the time, and style them separately:

	karuibar.modules              : ..., time_date, time_module
	karuibar.time_date.format     : %Y-%m-%d
	karuibar.time_date.foreground : #EEEEEE
	karuibar.time_date.background : #555555
	karuibar.time_time.format     : %H:%M
	karuibar.time_time.foreground : #555555
	karuibar.time_time.background : #EEEEEE

![profiles](scrot/profiles.png)

Each profile is a separate instance, so you can set any module-specific X
resource variable separately.

Profile names can be chosen arbitrarily, and they may contain all characters
except commas and semicolons (so yes, you could set a `time` profile like
`time_i?like_trains!`).


bugs
----

There certainly are plenty of them; feel free to [submit bug
reports](https://gitlab.com/ayekat/karuibar/issues).

*(I'm a hobby developer, I won't bite)*


copyright
---------

Copyright (c) 2015 Tinu Weber.
Licenced under the GPLv3 (http://gnu.org/licenses/gpl.html).
